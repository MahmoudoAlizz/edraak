import { Box, Center, Flex, Image } from "@mantine/core";
import CourseCard from "../components/course-card";
import data from "../../data/data.json";
const home = () => {
  let coursesInfo = data.courses.map((i) => {
    return {
      id: i.id,
      name: i.name,
      imgPath: i.imgPath,
    };
  });
  const renderCards = coursesInfo.map(
    (i: { id: number; imgPath: string; name: string }) => (
      <CourseCard
        key={i.id}
        id={i.id}
        imageSrc={i.imgPath}
        courseTitle={i.name}
      />
    )
  );
  return (
    <Box>
      <Center bg="white" mih="20vh" m="0">
        <Image w="20%" mah="100%" py="lg" mx="auto" src={"assets/logo.png"} />
      </Center>
      <Flex
        bg="#f8f9fa"
        mih={"80vh"}
        gap="xl"
        justify="center"
        align="center"
        direction="row"
        wrap="wrap"
      >
        {renderCards}
      </Flex>
    </Box>
  );
};

export default home;
