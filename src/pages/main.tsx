import Layout from "../components/layout";
import Lesson from "../components/lesson";
import Exam from "../components/exam";
import { useParams } from "react-router-dom";

const Course = () => {
  const { examId } = useParams();
  const renderView = () => {
    if (examId) {
      return <Exam />;
    } else {
      return <Lesson />;
    }
  };
  return <Layout>{renderView()}</Layout>;
};

export default Course;
