import Layout from "../components/layout";
import Lesson from "../components/lesson";

const Course = () => {
  return (
    <Layout>
      <Lesson />
    </Layout>
  );
};

export default Course;
