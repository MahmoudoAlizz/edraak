export type lessonProps = {
  id: number;
  title: string;
  lessonPath: string;
};
export type unitProps = {
  id: number;
  unitNum: string;
  lessons: lessonProps[];
  exam?: string;
};
export type courseProps = {
  id: number;
  name: string;
  imgPath: string;
  units: unitProps[];
};
