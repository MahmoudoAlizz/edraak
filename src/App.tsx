import { MantineProvider } from "@mantine/core";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import "@mantine/core/styles.css";
import Home from "./pages/home";
import Main from "./pages/main";
function App() {
  return (
    <MantineProvider
      theme={{
        fontFamily: "ReadexPro-Regular",
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route path="/" index element={<Home />} />
          <Route path="/course/:courseId" element={<Main />} />
          <Route path="/course/:courseId/lesson/:lessonId" element={<Main />} />
          <Route path="/course/:courseId/exam/:examId" element={<Main />} />
        </Routes>
      </BrowserRouter>
    </MantineProvider>
  );
}

export default App;
