import { AppShell, Burger, Center } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { ReactNode } from "react";
import Sidebar from "./sidebar";
import styles from "../../styles.module.css";
interface Props {
  children: ReactNode;
}
const index = ({ children }: Props) => {
  const [opened, { toggle }] = useDisclosure();

  return (
    <AppShell
      header={{ height: { base: 60, sm: 0 } }}
      aside={{
        width: { base: 300, md: 400 },
        breakpoint: "sm",
        collapsed: { mobile: !opened },
      }}
      padding="md"
    >
      <AppShell.Header>
        <Center h="100%" styles={{ root: { justifyContent: "end" } }}>
          <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />
        </Center>
      </AppShell.Header>
      <AppShell.Main>{children}</AppShell.Main>
      <AppShell.Aside className={styles.scrollAuto} bg="#f8f9fa">
        <Sidebar />
      </AppShell.Aside>
    </AppShell>
  );
};

export default index;
