import { Image, Box, Divider, Text } from "@mantine/core";
import { Link, useParams } from "react-router-dom";
import data from "../../../../data/data.json";
import UnitContent from "../../unit-content";
import style from "../../../styles.module.css";
import { courseProps } from "../../../types/data.type";
const index = () => {
  let { courseId } = useParams();
  let currentCourse = data.courses.find(
    (i: courseProps) => i.id == Number(courseId)
  );
  const renderUnits = currentCourse?.units.map((i) => (
    <UnitContent key={i.id} unit={i} />
  ));
  return (
    <Box py="md">
      <Link to={"/"}>
        <Image
          w={{ base: "25%", sm: "55%" }}
          py="lg"
          pr={{ base: "", sm: "xl" }}
          ml={{ base: "", sm: "auto" }}
          mx={{ base: "auto", sm: "0" }}
          src={"/assets/logo.png"}
        />
      </Link>
      <Divider w={"90%"} mx="auto" />
      <Link
        to={`/course/${courseId}`}
        className={style.link}
        style={{ color: "black" }}
      >
        <Text className={style.customFont} size="22px" ta={"center"} py="md">
          {currentCourse?.name}
        </Text>
      </Link>

      <Divider w={"90%"} mx="auto" />
      {renderUnits}
    </Box>
  );
};

export default index;
