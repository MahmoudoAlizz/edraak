import {
  Box,
  Button,
  Text,
  Collapse,
  Group,
  List,
  Flex,
  Center,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { IconChevronDown, IconChevronUp } from "@tabler/icons-react";
import { Link, useParams } from "react-router-dom";
import styles from "../styles.module.css";
import { lessonProps, unitProps } from "../types/data.type";
interface Props {
  unit: unitProps;
}
const unitsContent = ({ unit }: Props) => {
  const { courseId, lessonId, examId } = useParams();
  const [opened, { toggle }] = useDisclosure(
    unit.id == 1 || Number(examId) == unit.id ? true : false
  );

  const renderlessons = (
    <List
      mx="sm"
      styles={{
        root: { border: "1px solid #dee2e6" },
        item: { listStyleType: "none", textAlign: "right" },
      }}
    >
      {unit.lessons
        .sort((a: lessonProps, b: lessonProps) => a.id - b.id)
        .map((i: lessonProps) => (
          <List.Item
            key={i.id}
            px="sm"
            py={5}
            bg="white"
            style={{ border: "0.1px solid #dee2e6" }}
          >
            <Link
              className={styles.link}
              to={`/course/${courseId}/lesson/${i.id}`}
            >
              <Text
                className={Number(lessonId) == i.id ? styles.customFont : ""}
                c={Number(lessonId) == i.id ? "black" : ""}
              >
                {i.title}
              </Text>
            </Link>
          </List.Item>
        ))}
      {unit.exam ? (
        <List.Item
          px="sm"
          py={5}
          bg="white"
          style={{ border: "0.1px solid #dee2e6" }}
        >
          <Link
            to={`/course/${courseId}/exam/${unit.id}`}
            className={styles.link}
          >
            <Text
              className={Number(examId) == unit.id ? styles.customFont : ""}
              c={Number(examId) == unit.id ? "black" : ""}
            >
              الاختبار
            </Text>
          </Link>
        </List.Item>
      ) : (
        ""
      )}
    </List>
  );
  return (
    <Box my="xs">
      <Group justify="center" mb={5}>
        {/* <Flex w="100%" justify={"space-between"}>
          <Center>
            {opened ? (
              <IconChevronUp size="15px" />
            ) : (
              <IconChevronDown size="15px" />
            )}
          </Center>
          <Button
            styles={{ inner: { justifyContent: "end" } }}
            variant="subtle"
            fullWidth
            c="black"
            onClick={toggle}
          >
            {unit.unitNum}
          </Button>
        </Flex> */}
        <Button
          styles={{ label: { width: "100%" } }}
          variant="subtle"
          c="black"
          onClick={toggle}
          w="100%"
        >
          <Flex w="100%" justify={"space-between"}>
            <Center>
              {opened ? (
                <IconChevronUp size="15px" />
              ) : (
                <IconChevronDown size="15px" />
              )}
            </Center>
            <Box variant="subtle" c="black" onClick={toggle}>
              {unit.unitNum}
            </Box>
          </Flex>
        </Button>
      </Group>
      <Collapse in={opened}>{renderlessons}</Collapse>
    </Box>
  );
};

export default unitsContent;
