import { Center } from "@mantine/core";
import data from "../../data/data.json";
import { useParams } from "react-router-dom";
import { unitProps } from "../types/data.type";
const exam = () => {
  const { courseId, examId } = useParams();

  let unit: unitProps | undefined = data.courses
    .find((i) => i.id == Number(courseId))
    ?.units.find((j: unitProps) => j.id == Number(examId));

  return (
    <Center h={{ base: "80vh", sm: "90vh" }} w={"100%"} mt="xl">
      <iframe
        title={"PDF-Viewer"}
        src={unit?.exam}
        style={{ height: "100%", width: "90%" }}
      ></iframe>
    </Center>
  );
};

export default exam;
