import { Card, Center, Image, Text } from "@mantine/core";

interface Props {
  id: number;
  imageSrc: string;
  courseTitle: string;
}

const courseCard = ({ courseTitle, imageSrc, id }: Props) => {
  return (
    <Card
      shadow="sm"
      p="xs"
      component="a"
      href={`/course/${id}`}
      target="_self"
      w={{ base: 300, md: 500 }}
      h={{ base: 240, md: 340 }}
    >
      <Card.Section h={"85%"} m="0">
        <Center h="100%">
          <Image src={imageSrc} maw={"100%"} mah="100%" alt="" />
        </Center>
      </Card.Section>
      <Text ta={"center"} fw={500} size="lg" mt="md">
        {courseTitle}
      </Text>
    </Card>
  );
};

export default courseCard;
