import { Box } from "@mantine/core";
interface Props {
  videoPath: string | undefined;
}
const video = ({ videoPath }: Props) => {
  return (
    <div>
      <Box mt="md" mah={"80vh"} p="xl" key={videoPath}>
        <video autoPlay loop controls width={"100%"}>
          <source src={videoPath} type="video/mp4" />
        </video>
      </Box>
    </div>
  );
};

export default video;
