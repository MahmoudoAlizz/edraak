import { Box, Center, Flex, Text } from "@mantine/core";
import { Link, useParams } from "react-router-dom";
import data from "../../data/data.json";
import { IconArrowBackUp } from "@tabler/icons-react";
import { courseProps, lessonProps, unitProps } from "../types/data.type";
import Video from "./video";

const lesson = () => {
  const { courseId, lessonId } = useParams();
  if (!lessonId && courseId) {
    return (
      <Center h={{ base: "80vh", sm: "90vh" }} w={"100%"} mt="xl">
        <iframe
          title={"PDF-Viewer"}
          src={
            data.courses.find((i) => i.id == Number(courseId))?.descriptionFile
          }
          style={{ height: "100%", width: "90%" }}
        ></iframe>
      </Center>
    );
  }

  const lesson = data.courses
    .find((c: courseProps) => c.id == Number(courseId))
    ?.units.flatMap((unit: unitProps) => unit.lessons)
    .find((lesson: lessonProps) => lesson.id === Number(lessonId));

  return (
    <Box>
      <Flex
        gap="xl"
        justify={"space-between"}
        px={{ base: "0", md: "xl" }}
        mt="lg"
      >
        <Link to="/">
          <IconArrowBackUp color="#4d4d4d" stroke={4} size="30px" />
        </Link>
        <Center>
          <Text c={"#63b5b3"}> عنوان الدرس : {lesson?.title}</Text>
        </Center>
      </Flex>
      <Video videoPath={lesson?.lessonPath} />
    </Box>
  );
};

export default lesson;
